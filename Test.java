import java.io.*;

class Test
{
    public static void main(String[] args) {
        
        BufferedReader inp =  
        new BufferedReader(new InputStreamReader(System.in));
        int t = inp.nextInt();

        for(int x=0; x<t; x++)
        {
            int n = inp.nextInt();

            String res = "";

            int old = inp.nextInt();

            for(int i=0; i<n-1; i++)
            {
                int now = inp.nextInt();
                
                if(old>now)
                res += (now+" ");
                else
                res+= (-1+" ");

                old = now;
            }
            
            res += -1;
            System.out.println(res);
        }
    }
}