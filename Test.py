import itertools as it
import math

def secmax(l, f):
    ma = l[0]
    for i in range(len(l)):
        if l[i]>ma and l[i] < f:
            ma = l[i]
    return ma

def combi(l, a):
    count = 0
    ma = max(l)
    for i, j in it.combinations(range(len(l)+1), 2):
        b = l[i:j]
        # print(b)
        m = max(b)
        # print(secmax(b, m))
        # print("\n")
        if len(b)>=2 and secmax(b, m)<=a and (m == ma ):
            # print(secmax(b, m))
            # print(b)
            # print("n")
            count+=1
            print(b)
    return count


a = list(map(int, input().split()))

m = math.floor(max(a)//2)

res = combi(a, m)

print(res)
    

